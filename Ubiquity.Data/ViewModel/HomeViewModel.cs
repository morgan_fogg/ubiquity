using GalaSoft.MvvmLight;

namespace Ubiquity.Data.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class HomeViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        string profileName = "John Doe";
        string profileJobRole = "Software Consultant";
        string eventsTimeFrame = "Today";
        string eventsList = "- Hello\n- Hi\n- Howdy";
        string groupsTitle = "Groups";
        string alertsTitle = "Alerts";
        string alertsList = "- Alert1\n- Alert2\n- Alert3";

        public HomeViewModel()
        {
            ////if (IsInDesignMode)
            ////{
            ////    // Code runs in Blend --> create design time data.
            ////}
            ////else
            ////{
            ////    // Code runs "for real"
            ////}
        }

        public string ProfileName
        {
            get { return profileName; }
        }

        public string EventsTimeFrame
        {
            get { return eventsTimeFrame; }
        }

        public string EventsList
        {
            get { return eventsList; }
        }

        public string ProfileJobRole
        {
            get { return profileJobRole; }
        }

        public string GroupsTitle
        {
            get { return groupsTitle; }
        }

        public string AlertsTitle
        {
            get { return alertsTitle; }
        }

        public string AlertsList
        {
            get { return alertsList; }
        }
    }
}