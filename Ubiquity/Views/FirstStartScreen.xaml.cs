﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Ubiquity.Views
{
    public partial class FirstStartScreen : ContentPage
    {
        public FirstStartScreen()
        {
            InitializeComponent();
        }

        async void LogIn(object sender, EventArgs e)
        {
            LoginPage p = new LoginPage();
            await Navigation.PushModalAsync(p);

        }

        async void Register(object sender, EventArgs args)
        {
            await Navigation.PushModalAsync(new RegistrationPage());
        }
    }
}
