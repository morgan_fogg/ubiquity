﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Diagnostics;
using System.Collections.ObjectModel;

namespace Ubiquity
{
    public partial class ProfilePage : BaseContentPage
    {
        ObservableCollection<string> projects;
        public ProfilePage()
        {
            InitializeComponent();

            projects = new ObservableCollection<string>
                {
                    "Project A",
                    "Development B",
                    "Application C",
                    "Project D",
                    "Team E"
                };
            projectList.SetBinding(ListView.ItemsSourceProperty, new Binding("."));
            projectList.BindingContext = projects;
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
                return; 
            await Navigation.PushAsync(new ProjectPage());
        }
    }
}

