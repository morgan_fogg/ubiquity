﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Diagnostics;
using System.Collections.ObjectModel;

namespace Ubiquity
{
    public partial class ProjectPage : ContentPage
    {
        ObservableCollection<string> contacts;
        public ProjectPage()
        {
            InitializeComponent();

            contacts = new ObservableCollection<string>
                {
                    "Jenny Nguyen",
                    "Morgan Fogg",
                    "Muhammed Dogan",
                    "Dan Burton"
                };
            projectMembers.SetBinding(ListView.ItemsSourceProperty, new Binding("."));
            projectMembers.BindingContext = contacts;
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
                return; 

            var action = await DisplayActionSheet ("", "Cancel", "Delete", "Profile", "Call", "Message");
            switch (action){
                case "Profile":
                    await Navigation.PushAsync(new ProfilePage());
                    break;
            }
            //DisplayAlert("Tapped", e.SelectedItem + " row was tapped", "OK");
            //((ListView)sender).SelectedItem = null;
        }

        public void OnDelete (object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);
            DisplayAlert("Delete Context Action", mi.CommandParameter + " delete context action", "OK");
            Debug.WriteLine("delete " + mi.CommandParameter.ToString());
            contacts.Remove(mi.CommandParameter.ToString());
        }
    }
}

