﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace Ubiquity
{
    public partial class HomePage : BaseMasterDetailPage
	{
        protected override void OnAppearing ()
        {
            base.OnAppearing ();
        }

        void logout_click(Object sender, EventArgs e)
        {
            //logout of app, sets the StoredToken and Saved File to null, and calls BaseContentPage Again
            DependencyService.Get<Ubiquity.App.ISaveAndLoad>().SaveText("token", null);
            App.StoredToken = null;
            Navigation.PushModalAsync(new BaseMasterDetailPage());
        }

        void NavigateTo (NavMenuItem menu) {
            Page displayPage = (Page)Activator.CreateInstance (menu.TargetType);

            Detail = new NavigationPage (displayPage);

            IsPresented = false;
        }

		public HomePage ()
        {
			InitializeComponent();
            BindingContext = App.Locator.Home;
            this.Title = "Ubiquity Home";

            UpdateGroupsList(); 


            var navMenuPage = new NavMenuPage ();

            navMenuPage.Menu.ItemSelected += (sender, e) => NavigateTo (e.SelectedItem as NavMenuItem);

            Master = navMenuPage;

        }

        public void UpdateGroupsList()
        {
            this.GroupsList.Children.Clear();
            this.GroupsList.Children.Add(new StackLayout { Padding=5, BackgroundColor=Color.White, Children = { new Label { Text = "Group1", TextColor = Color.Black}, new Label {Text = "No New Notifications", TextColor=Color.Gray} } });
            this.GroupsList.Children.Add(new BoxView { WidthRequest = 2 });
            this.GroupsList.Children.Add(new StackLayout { Padding = 5, BackgroundColor = Color.White, Children = { new Label { Text = "Group1", TextColor = Color.Black }, new Label { Text = "No New Notifications", TextColor = Color.Gray } } });
            this.GroupsList.Children.Add(new BoxView { WidthRequest = 2 });
            this.GroupsList.Children.Add(new StackLayout { Padding = 5, BackgroundColor = Color.White, Children = { new Label { Text = "Group1", TextColor = Color.Black }, new Label { Text = "No New Notifications", TextColor = Color.Gray } } });
        }


    }	
}

