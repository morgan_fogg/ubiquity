﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Ubiquity
{
    public partial class DepartmentPage : ContentPage
    {
        public DepartmentPage()
        {
            InitializeComponent();

            DepartmentMembersListView.ItemsSource = new string[]{
                "Jenny Nguyen",
                "Morgan Fogg",
                "Muhammed Dogan",
                "Dan Burton",
                "Jane Doe",
                "Jane Doe",
                "Jane Doe",
                "Jane Doe",
                "Jane Doe",
                "Jane Doe"
            };

        }
    }
}

