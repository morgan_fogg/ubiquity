﻿using System;
using Ubiquity.Data.ViewModel;
using Xamarin.Forms;

namespace Ubiquity
{
	public class App : Application
	{
        public static ViewModelLocator Locator;
		public App ()
		{
            Locator = new ViewModelLocator();
			// The root page of your application
            MainPage = new NavigationPage( new Ubiquity.HomePage());
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

