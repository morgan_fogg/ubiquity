﻿using System;
using Ubiquity.Model;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ubiquity
{
    public class ProjectViewModel
    {
        public ObservableCollection<Project> Projects { get; set; }
        //public ObservableCollection<Grouping<string, Project>> MonkeysGrouped { get; set; }

        public ProjectViewModel()
        {

            Projects = new ObservableCollection<Project>();
            Projects.Add(new Project
                {
                    Name = "Project 1",
                    TestString = "This text should be different from project 2"
                });

            Projects.Add(new Project
                {
                    Name = "Project 2",
                    TestString = "blablabla different from project 1"
                });

            //var sorted = from Project in Projects
               // orderby Project.Name
            //    group Project by Project.NameSort into monkeyGroup
             //   select new Grouping<string, Monkey>(monkeyGroup.Key, monkeyGroup);

           // MonkeysGrouped = new ObservableCollection<Grouping<string, Monkey>>(sorted);
        }
    }
}

