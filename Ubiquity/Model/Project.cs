﻿using System;

namespace Ubiquity.Model
{
    public class Project
    {
        public string Name {get;set;}
        public string TestString { get; set; }

        public string NameSort
        {
            get 
            {
                if (string.IsNullOrWhiteSpace(Name) || Name.Length == 0)
                    return "?";

                return Name[0].ToString().ToUpper();
            }
        }

    }
}

