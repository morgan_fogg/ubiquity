﻿using System;

using Xamarin.Forms;

namespace Ubiquity
{
    public class BaseMasterDetailPage : ContentPage
    {
        public BaseMasterDetailPage()
        {
            Content = new StackLayout
            { 
                Children =
                {
                    new Label { Text = "Hello ContentPage" }
                }
            };
        }
    }
}


