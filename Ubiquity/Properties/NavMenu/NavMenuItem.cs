﻿using System;

using Xamarin.Forms;

namespace Ubiquity
{
    public class NavMenuItem : ContentPage
    {
        public NavMenuItem()
        {
            Content = new StackLayout
            { 
                Children =
                {
                    new Label { Text = "Hello ContentPage" }
                }
            };
        }
    }
}


