﻿using System;

using Xamarin.Forms;

namespace Ubiquity
{
    public class NavMenu : ContentPage
    {
        public NavMenu()
        {
            Content = new StackLayout
            { 
                Children =
                {
                    new Label { Text = "Hello ContentPage" }
                }
            };
        }
    }
}


