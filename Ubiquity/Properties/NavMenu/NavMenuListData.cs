﻿using System;

using Xamarin.Forms;

namespace Ubiquity
{
    public class NavMenuListData : ContentPage
    {
        public NavMenuListData()
        {
            Content = new StackLayout
            { 
                Children =
                {
                    new Label { Text = "Hello ContentPage" }
                }
            };
        }
    }
}


