﻿using System;

using Xamarin.Forms;

namespace Ubiquity
{
    public class NavMenuListView : ContentPage
    {
        public NavMenuListView()
        {
            Content = new StackLayout
            { 
                Children =
                {
                    new Label { Text = "Hello ContentPage" }
                }
            };
        }
    }
}


