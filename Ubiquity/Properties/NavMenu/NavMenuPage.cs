﻿using System;

using Xamarin.Forms;

namespace Ubiquity
{
    public class NavMenuPage : ContentPage
    {
        public NavMenuPage()
        {
            Content = new StackLayout
            { 
                Children =
                {
                    new Label { Text = "Hello ContentPage" }
                }
            };
        }
    }
}


