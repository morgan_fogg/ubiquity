﻿using System;

namespace Ubiquity.Netcode
{
    public static class Login
    {
        private static bool isLoggedIn = false;

        public static bool IsLoggedIn
        {
            get {return isLoggedIn;}
        }

        public static void LogIn()
        {
            isLoggedIn = true;
        }
    }
}

